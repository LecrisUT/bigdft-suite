#ifndef DYNAMIC_MEMORY
#define DYNAMIC_MEMORY

#include "futile_cst.h"
#include <iterator>
#include <array>

namespace Futile {

template <typename T>
class NumericIterator : public std::iterator<std::random_access_iterator_tag, T>
{
public:
    NumericIterator(T *data, size_t step = 1) : mData(data), mStep(step) {}
    NumericIterator(const NumericIterator<T> &other) : mData(other.mData) {}
    NumericIterator<T> &operator++() {mData += mStep; return *this;}
    bool operator==(const NumericIterator<T> &other) const {return mData == other.mData;}
    bool operator!=(const NumericIterator<T> &other) const {return mData != other.mData;}
    T& operator*() {return *mData;}
private:
    T *mData;
    size_t mStep;
};

template <typename T>
class NumericSlice
{
public:
    NumericSlice(T *data, size_t offset, size_t size)
        : mData(data), mOffset(offset), mSize(size) {}
    NumericIterator<T> begin() {return NumericIterator<T>(mData, mOffset);}
    NumericIterator<T> end() {return NumericIterator<T>(mData + mSize);}
private:
    T *mData;
    size_t mOffset;
    size_t mSize;
};

template <typename T, unsigned int D>
class NumericPointer
{
protected:
    F_DEFINE_TYPE(numeric);
    NumericPointer(f90_numeric_pointer other, size_t *len)
        : mPointer(other)
    {
        mData = (T*)F_TYPE(mPointer);
        mSize = 1;
        for (unsigned int i = 0; i < D; i++) {
            mShape[i] = len[i];
            mSize *= len[i];
        }
    }
    NumericPointer(f90_numeric_pointer other, size_t len)
        : mPointer(other), mSize(len)
    {
        mData = (T*)F_TYPE(mPointer);
        mShape[0] = len;
    }

    NumericPointer() : mData(NULL), mSize(0) {}
    ~NumericPointer() {}

    NumericPointer<T, D>& operator=(NumericPointer<T, D> &&other)
    {
        mPointer = other.mPointer;
        mData = other.mData;
        for (unsigned int i = 0; i < D; i++) mShape[i] = other.mShape[i];
        mSize = other.mSize;
        other.mData = NULL;
        return *this;
    }

    static const unsigned int mDims = D;
    f90_numeric_pointer mPointer;
    T *mData;
    size_t mShape[D];
    size_t mSize;
public:
    T& operator[](size_t at) {return at < mSize ? mData[at] : mData[at % mSize];}
    T& operator[](const std::array<size_t, D> &at)
    {
        size_t offset = 0;
        for (unsigned int i = 0; i < D; i++) {
            offset *= mShape[i];
            offset += at[i] < mShape[i] ? at[i] : (mShape[i] - 1);
        }
        return mData[offset];
    }
    size_t size() const {return mSize;}
    size_t shape(unsigned int dir) const {return dir < D ? mShape[dir] : 0;}
    NumericSlice<T> sliced() {return NumericSlice<T>(mData, mShape[0], mSize);}

    NumericIterator<T> begin() {return NumericIterator<T>(mData);}
    NumericIterator<T> end() {return NumericIterator<T>(mData + mSize);}
};
    
class Int1dPointer : public NumericPointer<int, 1>
{
public:
    typedef NumericPointer::f90_numeric_pointer f90_int_1d_pointer;
    typedef NumericIterator<int> Iterator;

    Int1dPointer() : NumericPointer() {}
    Int1dPointer(f90_int_1d_pointer other, size_t len)
        : NumericPointer(other, len) {}
    ~Int1dPointer();

    Int1dPointer& operator=(Int1dPointer &&) = default;
};

class Double2dPointer : public NumericPointer<double, 2>
{
public:
    typedef NumericPointer::f90_numeric_pointer f90_double_2d_pointer;
    typedef NumericIterator<double> Iterator;

    Double2dPointer() : NumericPointer() {}
    Double2dPointer(f90_double_2d_pointer other, size_t len[2])
        : NumericPointer(other, len) {}
    ~Double2dPointer();

    Double2dPointer& operator=(Double2dPointer &&) = default;
};

}
#endif

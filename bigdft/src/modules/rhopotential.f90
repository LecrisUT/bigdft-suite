!> @file
!! Handle operation about density (rho) and local potential
!! @author
!!   Copyright (C) 2011-2015 BigDFT group
!!   This file is distributed under the terms of the
!!   GNU General Public License, see ~/COPYING file
!!   or http://www.gnu.org/copyleft/gpl.txt .
!!   For the list of contributors, see ~/AUTHORS


!> Handle operation about density (rho) and local potential
module rhopotential
  use f_precisions, only: f_long
  use module_precisions
  use Poisson_Solver, only: coulomb_operator
  use module_xc, only: xc_info
  use locregs, only: locreg_descriptors
  use communications_base, only: p2pcomms
  use module_dpbox, only: denspot_distribution
  use module_input_keys, only: SIC_data

  implicit none

  private

  public :: updatePotential
  public :: sumrho_for_TMBs
  public :: clean_rho
  public :: corrections_for_negative_charge
  public :: extract_potential
  public :: exchange_and_correlation,set_cfd_data

  type, public :: local_operator
     integer :: npot = 1 !< number of potential components (1 or 4)
     real(wp), dimension(:), pointer :: pot => null() !< Orbital independant potential
     real(wp), dimension(:), pointer :: odp => null() !< Orbital dependant potential

     logical :: ownPot !< A flag to deallocate pot
     integer(f_long), dimension(:), pointer :: ispot !< Starting index in pot for every orbitals, size norbp
     integer(f_long) :: isexctX !< Starting index in odp for exact exchange odp
     integer(f_long) :: issic !< Starting index in odp for sic odp

     integer(f_long) :: psirDim !< Maximum size to expand psi on mesh

     real(wp), dimension(:), pointer :: vpsi_noconf => null() !< Stores the result of local operator without confinement
     real(wp), dimension(:), pointer :: distributedPotential => null() !< Typically rhov
     type(locreg_descriptors) :: glr
     type(denspot_distribution) :: dpbox
     type(p2pcomms) :: comgp
     type(SIC_data) :: SIC
     type(xc_info) :: xc
     type(coulomb_operator) :: pkernel
     real(gp) :: exctXcoeff = 0._gp
  end type local_operator

  public :: local_operator_new, local_operator_free
  public :: local_operator_gather

  type, public :: local_potential_iterator
     integer :: npot, ispin, ilr
     real(wp), dimension(:), pointer :: pot => null()
     real(wp), dimension(:), pointer :: potSIC => null()
     real(wp), dimension(:), pointer :: potExctX => null()

     !> workarrays for local potential application
     real(wp), dimension(:), pointer :: psir => null()
     real(wp), dimension(:), pointer :: psir_noconf => null()
     real(wp), dimension(:), pointer :: vsicpsir => null()
     
     type(local_operator), pointer :: parent
  end type local_potential_iterator

  public :: local_potential_iter_new, local_potential_iter_free
  public :: local_potential_iter_ensure_pot

  contains

    !> Calculates the KS potential and energy
    subroutine updatePotential(nspin,denspot,energs)!ehart,eexcu,vexcu)

    use module_precisions
    use module_types
    use module_interfaces, only: XC_potential
    use public_enums, only: ELECTRONIC_DENSITY, KS_POTENTIAL
    use Poisson_Solver, except_dp => dp, except_gp => gp
    use yaml_output
    !use box
    use PSbox
    use f_enums
    use at_domain, only: domain_geocode
    use module_bigdft_arrays
    use wrapper_linalg
    use module_bigdft_profiling
    implicit none

    ! Calling arguments
    integer, intent(in) :: nspin                     !< Spin number
    type(DFT_local_fields), intent(inout) :: denspot !< in=density, out=KS potential
    type(energy_terms), intent(inout) :: energs
    !real(kind=8), intent(out) :: ehart, eexcu, vexcu !> Energies (Hartree, XC and XC potential energy)

    ! Local variables
    character(len=*), parameter :: subname='updatePotential'
    logical :: nullifyVXC
    real(gp) :: ehart_ps
    real(dp), dimension(6) :: xcstr

    call f_routine(id='updatePotential')

    nullifyVXC=.false.

    ! rhov should be the electronic density
    !todo: the sumrho for TMB should update the rhov_is status, in the linear scaling case
    !if (denspot%rhov_is /= ELECTRONIC_DENSITY) &
    !      call f_err_throw('updatePotential: ELECTRONIC_DENSITY not available, control the operations on rhov',&
    !    err_name='BIGDFT_RUNTIME_ERROR')

    if(nspin==4) then
       !this wrapper can be inserted inside the poisson solver
       !call PSolverNC(domain_geocode(denspot%dpbox%mesh%dom),'D',denspot%pkernel%mpi_env%iproc,denspot%pkernel%mpi_env%nproc,&
       call PSolverNC(denspot%dpbox%mesh%dom,'D',denspot%pkernel%mpi_env%iproc,denspot%pkernel%mpi_env%nproc,&
            denspot%dpbox%mesh%ndims(1),denspot%dpbox%mesh%ndims(2),&
            denspot%dpbox%mesh%ndims(3),&
            denspot%dpbox%n3d,denspot%xc,&
            denspot%dpbox%mesh%hgrids,&
            denspot%rhov,denspot%pkernel%kernel,denspot%V_ext,energs%eh,energs%exc,energs%evxc,0.d0,.true.,4)

    else
       if (.not. associated(denspot%V_XC)) then
          !Allocate XC potential
          if (denspot%dpbox%n3p >0) then
             denspot%V_XC = f_malloc_ptr((/ denspot%dpbox%mesh%ndims(1),&
                  denspot%dpbox%mesh%ndims(2),&
                  denspot%dpbox%n3p , nspin /),&
                                id='denspot%V_XC')
          else
             denspot%V_XC = f_malloc_ptr((/ 1 , 1 , 1 , 1 /),id='denspot%V_XC')
          end if
          nullifyVXC=.true.
       end if

       call XC_potential(domain_geocode(denspot%dpbox%mesh%dom),'D',denspot%pkernel%mpi_env%iproc,denspot%pkernel%mpi_env%nproc,&
            denspot%pkernel%mpi_env%mpi_comm,&
            denspot%dpbox%mesh%ndims(1),denspot%dpbox%mesh%ndims(2),denspot%dpbox%mesh%ndims(3),denspot%xc,&
            denspot%dpbox%mesh%hgrids,&
            denspot%rhov,energs%exc,energs%evxc,nspin,denspot%rho_C,denspot%rhohat,denspot%V_XC,xcstr)

       call H_potential('D',denspot%pkernel,denspot%rhov,denspot%V_ext,ehart_ps,0.0_dp,.true.,&
            quiet=denspot%PSquiet,rho_ion=denspot%rho_ion) !optional argument

       if (denspot%pkernel%method /= 'VAC') then
          energs%eelec=ehart_ps
          energs%eh=0.0_gp
       else
          energs%eelec=0.0_gp
          energs%eh=ehart_ps
       end if

       !sum the two potentials in rhopot array
       !fill the other part, for spin, polarised
       if (nspin == 2) then
          !denspot%dpbox%ndims(1)*denspot%dpbox%ndims(2)*denspot%dpbox%n3p
          call vcopy(denspot%dpbox%ndimpot,denspot%rhov(1),1,&
               denspot%rhov(1+denspot%dpbox%ndimpot),1)
       end if
       !spin up and down together with the XC part
       !denspot%dpbox%ndims(1)*denspot%dpbox%ndims(2)*denspot%dpbox%n3p
       call axpy(denspot%dpbox%ndimpot*nspin,1.0_dp,denspot%V_XC(1,1,1,1),1,&
            denspot%rhov(1),1)

       if (nullifyVXC) call f_free_ptr(denspot%V_XC)

    end if

    !now the meaning is KS potential
    call denspot_set_rhov_status(denspot, KS_POTENTIAL, 0, denspot%pkernel%mpi_env%iproc,denspot%pkernel%mpi_env%nproc)

    call f_release_routine()

    END SUBROUTINE updatePotential


    function lo_alloc_max_llr(ispot, llr, orbs) result(pot)
      use f_precisions
      use module_bigdft_arrays
      use module_types
      use locregs
      implicit none
      real(wp), dimension(:), pointer :: pot
      type(orbitals_data), intent(in) :: orbs
      integer(f_long), dimension(orbs%norbp), intent(out) :: ispot
      type(locreg_descriptors), dimension(:), intent(in) :: llr

      integer :: iorb, ilr
      integer(f_long) :: potdim
      
      ! Max of llr mesh dimensions, need to extract from comgp for each locreg
      potdim = 0
      do iorb = 1, orbs%norbp
         ilr = orbs%inwhichlocreg(iorb + orbs%isorb)
         potdim = max(potdim, Llr(ilr)%mesh%ndim)
         ispot(iorb) = 1_f_long
      end do
      if (orbs%nspinor == 4) potdim = potdim * 4
      pot = f_malloc_ptr(potdim, id = "pot")
    end function lo_alloc_max_llr

    function lo_alloc_and_setup_for_llr(ispot, glr, llr, orbs, full) result(pot)
      use f_precisions
      use module_precisions
      use module_bigdft_arrays
      use module_types
      use locregs
      use locreg_operations
      implicit none
      real(wp), dimension(:), pointer :: pot
      type(orbitals_data), intent(in) :: orbs
      integer(f_long), dimension(orbs%norbp), intent(out) :: ispot
      type(locreg_descriptors), intent(in) :: glr
      type(locreg_descriptors), dimension(:), intent(in) :: llr
      real(wp), dimension(:), pointer :: full

      integer :: npot, iorb, jorb, ilr
      integer(f_long) :: potdim, istl
      real(wp), dimension(:), pointer :: subpot, subfull

      npot = 1
      if (orbs%nspinor == 4) npot = 4
      ! Sum of all inequivalent llr mesh dimensions
      potdim = 0
      do iorb = 1, orbs%norbp
         ilr = orbs%inwhichlocreg(iorb + orbs%isorb)
         ispot(iorb) = potdim + 1
         do jorb = 1, iorb - 1
            if (orbs%inwhichlocreg(jorb + orbs%isorb) == ilr .and. &
                 orbs%spinsgn(iorb + orbs%isorb) == orbs%spinsgn(jorb + orbs%isorb)) then
               ispot(iorb) = ispot(jorb)
               exit
            end if
         end do
         if (jorb == iorb) &
              potdim = potdim + Llr(ilr)%mesh%ndim * npot
      end do
      pot = f_malloc_ptr(potdim, id = "pot")

      ! Cut potential
      istl = 0
      do iorb = 1, orbs%norbp
         if (ispot(iorb) /= istl) then
            istl = ispot(iorb)
            ! Cut the potential into locreg pieces
            ilr = orbs%inwhichlocreg(iorb + orbs%isorb)
            subpot => f_subptr(pot, from = ispot(iorb), &
                 size = Llr(ilr)%mesh%ndim * npot)
            if (orbs%spinsgn(iorb + orbs%isorb) > 0._gp) then
               subfull => f_subptr(full(1), size = glr%mesh%ndim * npot)
            else
               subfull => f_subptr(full(glr%mesh%ndim+1), size = glr%mesh%ndim * npot)
            end if
            call global_to_local(Glr, Llr(ilr),npot, &
                 int(glr%mesh%ndim) * npot, &
                 int(Llr(ilr)%mesh%ndim) * npot, subfull, subpot)
         end if
      end do
    end function lo_alloc_and_setup_for_llr

    subroutine lo_setup_for_glr(ispot, glr, orbs)
      use f_precisions
      use module_precisions
      use module_types
      use locregs
      implicit none
      type(orbitals_data), intent(in) :: orbs
      integer(f_long), dimension(orbs%norbp), intent(out) :: ispot
      type(locreg_descriptors), intent(in) :: glr

      integer :: iorb
      
      do iorb = 1, orbs%norbp
         ispot(iorb) = 1_f_long
         if(orbs%spinsgn(orbs%isorb+iorb) <= 0.0_gp) &
              ispot(iorb) = glr%mesh%ndim + 1
      end do
    end subroutine lo_setup_for_glr

    function local_operator_new(glr, orbs, dpbox, &
         xc, pkernel, SIC, potential, comgp, vpsi_noconf) result(lt)
      use module_precisions
      use module_xc
      use module_types
      use locregs
      use Poisson_Solver, only: coulomb_operator
      use module_dpbox
      use communications_base
      use communications
      use module_bigdft_mpi
      implicit none
      type(local_operator) :: lt
      type(locreg_descriptors), intent(in) :: glr
      type(orbitals_data), intent(in) :: orbs
      type(p2pcomms), intent(inout), optional :: comgp
      type(denspot_distribution), intent(in) :: dpbox
      real(wp), dimension(max(dpbox%ndimrhopot,dpbox%nrhodim)), intent(in), target :: potential
      type(xc_info), intent(in) :: xc
      type(coulomb_operator), intent(in) :: pkernel
      type(SIC_data), intent(in) :: SIC
      real(wp), dimension(:), intent(inout), optional, target :: vpsi_noconf

      integer :: ilr, iorb

      lt%npot = 1
      if (orbs%nspinor == 4) lt%npot = 4

      lt%comgp = p2pComms_null()
      if (present(comgp)) then
         call start_onesided_communication(bigdft_mpi%iproc, bigdft_mpi%nproc, &
              dpbox%ngatherarr, potential, comgp%nrecvbuf * comgp%nspin, &
              comgp%recvbuf, comgp, glr)
         lt%comgp = comgp
         lt%glr = glr
      else
         lt%distributedPotential => potential
         lt%dpbox = dpbox
      end if
         
      ! Prepare orbital dependant potential and metadata.
      nullify(lt%pkernel%kernel)
      lt%exctXcoeff = xc_exctXfac(xc)
      lt%SIC = SIC

      if (lt%exctXcoeff /= 0._gp) then
         ! Shallow copy of metadata used when building the exact exchange potentials
         lt%dpbox = dpbox
         lt%pkernel = pkernel
         lt%glr = glr
      end if
      if (lt%SIC%alpha /= 0._gp) then
         ! Shallow copies of metadata used to compute the SIC potentials
         lt%xc = xc
         lt%pkernel = pkernel
         lt%glr = glr
      end if

      if (present(vpsi_noconf)) lt%vpsi_noconf => vpsi_noconf
    end function local_operator_new

    subroutine local_operator_gather(lt, lzd, orbs)
      use module_precisions
      use module_xc
      use module_types
      use Poisson_Solver, only: coulomb_operator
      use module_dpbox
      use communications_base
      use communications
      use module_bigdft_arrays
      use module_bigdft_mpi
      use module_bigdft_profiling
      use wrapper_linalg
      implicit none
      type(local_operator), intent(inout) :: lt
      type(local_zone_descriptors), intent(in) :: lzd
      type(orbitals_data), intent(in) :: orbs

      integer :: ispin, ilr, iorb
      integer(f_long) :: potdim, isfull, ispotential
      real(wp), dimension(:), pointer :: subpot, subfull
      real(wp), dimension(:), pointer :: full

      call timing(bigdft_mpi%iproc,'Pot_commun    ','ON')
      if (associated(lt%comgp%recvBuf)) then
         call synchronize_onesided_communication(bigdft_mpi%iproc, bigdft_mpi%nproc, lt%comgp)
      else if (associated(lt%distributedPotential)) then
         if (bigdft_mpi%nproc > 1) then
            full = f_malloc_ptr(lt%dpbox%mesh%ndim * orbs%nspin, id = "pot")
            isfull = 1
            ispotential = 1
            do ispin = 1, orbs%nspin
               subfull => f_subptr(full(isfull), size = lt%dpbox%mesh%ndim)
               subpot => f_subptr(lt%distributedPotential(ispotential), size = lt%dpbox%ndimpot)
               call dpbox_gather(lt%dpbox, subfull, subpot, mpi_env=lt%dpbox%mpi_env)
               isfull = isfull + lt%dpbox%mesh%ndim
               ispotential = ispotential + lt%dpbox%ndimpot
            end do
         else
            full => lt%distributedPotential
         end if
      end if
      call timing(bigdft_mpi%iproc,'Pot_commun    ','OF')
         
      ! Prepare orbital independant potential.
      call timing(bigdft_mpi%iproc,'Pot_after_comm','ON')
      lt%ispot = f_malloc_ptr(orbs%norbp, id = "lt%ispot")
      lt%ownPot = .true.
      if (associated(lt%comgp%recvBuf)) then
         lt%pot => lo_alloc_max_llr(lt%ispot, lzd%llr, orbs)
      else if (lzd%linear) then
         lt%pot => lo_alloc_and_setup_for_llr(lt%ispot, lzd%glr, lzd%llr, orbs, full)
         if (bigdft_mpi%nproc > 1) call f_free_ptr(full)
      else
         call lo_setup_for_glr(lt%ispot, lzd%glr, orbs)
         lt%pot => full
         lt%ownPot = (bigdft_mpi%nproc > 1)
      end if
      call timing(bigdft_mpi%iproc,'Pot_after_comm','OF')

      potdim = 0
      if (lt%exctXcoeff /= 0._gp) then
         lt%isexctX = potdim + 1
         ! Valid only in Glr case
         potdim = potdim + dpbox_exctx_dim(lt%dpbox, orbs%norb, orbs%norbp)
      else
         lt%isexctX = 0
      end if
      if (lt%SIC%alpha /= 0._gp) then
         lt%issic = potdim + 1
         ! Valid only in Glr case
         potdim = potdim + lzd%glr%mesh%ndim * max(orbs%norbp, orbs%nspin)
      else
         lt%issic = 0
      end if

      lt%odp = f_malloc_ptr(potdim, id = "lt%odp")
      if (lt%SIC%alpha /= 0._gp .and. lt%dpbox%i3rho_add > 0) then
         ! Copy the density from the second part of potential
         call timing(bigdft_mpi%iproc,'Pot_commun    ','ON')
         if (bigdft_mpi%nproc > 1) then
            ispotential = lt%dpbox%mesh%ndim * orbs%nspin
            isfull = lt%issic
            do ispin = 1, orbs%nspin
               subfull => f_subptr(lt%odp(isfull), size = lt%dpbox%mesh%ndim)
               subpot => f_subptr(lt%distributedPotential(ispotential), size = lt%dpbox%ndimpot)
               call dpbox_gather(lt%dpbox, subfull, subpot, mpi_env=lt%dpbox%mpi_env)
               isfull = isfull + lt%dpbox%mesh%ndim
               ispotential = ispotential + lt%dpbox%ndimpot
            end do
         else
            potdim = lt%dpbox%mesh%ndim * orbs%nspin
            call vcopy(int(potdim), lt%distributedPotential(potdim+lt%dpbox%i3rho_add+1),1, &
                 lt%odp(lt%issic),1)
         end if
         call timing(bigdft_mpi%iproc,'Pot_commun    ','OF')
      end if

      nullify(lt%distributedPotential)

      ! Compute work-arrays sizes used by the local operator
      ! to expand the wavefunctions into mesh grid.
      lt%psirDim = 0
      do iorb = 1, orbs%norbp
         ilr = orbs%inwhichlocreg(iorb + orbs%isorb)
         lt%psirDim = max(lt%psirDim, lzd%Llr(ilr)%mesh%ndim)
      end do
      lt%psirDim = lt%psirDim * orbs%nspinor
    end subroutine local_operator_gather

    subroutine local_operator_free(lt, comgp)
      use module_bigdft_arrays
      implicit none
      type(local_operator), intent(inout) :: lt
      type(p2pcomms), intent(inout), optional :: comgp

      call f_free_ptr(lt%ispot)
      nullify(lt%ispot)
      if (lt%ownPot) call f_free_ptr(lt%pot)
      nullify(lt%pot)
      call f_free_ptr(lt%odp)
      nullify(lt%odp)
      if (present(comgp)) comgp = lt%comgp
    end subroutine local_operator_free

    function local_operator_get_pot_for_orbital(lt, iorbp, llr) result(pot)
      use module_precisions
      use module_bigdft_arrays
      use locregs
      implicit none
      real(wp), dimension(:), pointer :: pot
      type(local_operator), intent(in) :: lt
      integer, intent(in) :: iorbp
      type(locreg_descriptors), intent(in) :: llr

      if (iorbp > 0 .and. iorbp <= size(lt%ispot)) then
         pot => f_subptr(lt%pot(lt%ispot(iorbp)), size = llr%mesh%ndim * lt%npot)
      else
         nullify(pot)
      end if
    end function local_operator_get_pot_for_orbital
    
    function local_operator_get_SIC_for_orbital(lt, iorbp) result(pot)
      use module_precisions
      use module_bigdft_arrays
      use locregs
      implicit none
      real(wp), dimension(:), pointer :: pot
      type(local_operator), intent(in) :: lt
      integer, intent(in) :: iorbp

      if (lt%SIC%alpha /= 0._gp .and. lt%SIC%approach == 'NK') then
         pot => f_subptr(lt%odp(lt%issic + (iorbp-1) * lt%glr%mesh%ndim * lt%npot), &
              size = lt%glr%mesh%ndim * lt%npot)
      else
         nullify(pot)
      end if
    end function local_operator_get_SIC_for_orbital
    
    function local_operator_get_exctX_for_orbital(lt, iorbp) result(pot)
      use module_precisions
      use module_bigdft_arrays
      use locregs
      implicit none
      real(wp), dimension(:), pointer :: pot
      type(local_operator), intent(in) :: lt
      integer, intent(in) :: iorbp

      if (lt%exctXcoeff /= 0._gp) then
         pot => f_subptr(lt%odp(lt%isexctX + (iorbp-1) * lt%glr%mesh%ndim * lt%npot), &
              size = lt%glr%mesh%ndim * lt%npot)
      else
         nullify(pot)
      end if
    end function local_operator_get_exctX_for_orbital
    
    function local_potential_iter_new(lt) result(pot_it)
      use module_bigdft_arrays
      implicit none
      type(local_potential_iterator) :: pot_it
      type(local_operator), intent(in), target :: lt

      pot_it%parent => lt
      pot_it%npot = lt%npot

      ! Allocate work-arrays used by the local operator
      ! to expand the wavefunctions into mesh grid.
      pot_it%psir = f_malloc_ptr(lt%psirDim, id = 'psir')
      if (associated(lt%vpsi_noconf)) then
         pot_it%psir_noconf = f_malloc_ptr(lt%psirDim, id = 'psir_noconf')
      end if
      if (lt%SIC%alpha /= 0._gp) then
         pot_it%vsicpsir = f_malloc_ptr(lt%psirDim, id = 'vsicpsir')
      end if
    end function local_potential_iter_new

    subroutine local_potential_iter_free(pot_it)
      use module_bigdft_arrays
      implicit none
      type(local_potential_iterator), intent(inout) :: pot_it

      if (associated(pot_it%psir)) call f_free_ptr(pot_it%psir)
      if (associated(pot_it%psir_noconf)) call f_free_ptr(pot_it%psir_noconf)
      if (associated(pot_it%vsicpsir)) call f_free_ptr(pot_it%vsicpsir)
    end subroutine local_potential_iter_free

    subroutine local_potential_iter_ensure_pot(pot, lr, iorbp, ilr, ispin)
      use module_bigdft_arrays
      use locregs
      implicit none
      type(local_potential_iterator), intent(inout) :: pot
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: iorbp, ilr, ispin

      pot%pot => local_operator_get_pot_for_orbital(pot%parent, iorbp, lr)

      if ((pot%ilr /= ilr .or. pot%ispin /= ispin) .and. associated(pot%parent%comgp%recvBuf)) then
         call extract_potential(ispin, lr, pot%parent%glr, pot%pot, pot%parent%comgp)
      end if
      pot%ispin = ispin
      pot%ilr = ilr

      pot%potSIC => local_operator_get_SIC_for_orbital(pot%parent, iorbp)
      pot%potExctX => local_operator_get_exctX_for_orbital(pot%parent, iorbp)
    end subroutine local_potential_iter_ensure_pot

    subroutine sumrho_for_TMBs(iproc, nproc, hx, hy, hz, collcom_sr, denskern, aux, denskern_, ndimrho, rho, &
            rho_negative, print_results)
      use module_bigdft_arrays
      use wrapper_linalg
      use module_bigdft_mpi
      use yaml_output
      use sparsematrix_base, only: sparse_matrix, matrices
      use module_types, only: linmat_auxiliary, comms_linear
      use module_bigdft_profiling
      implicit none

      ! Calling arguments
      integer,intent(in) :: iproc, nproc, ndimrho
      real(kind=8),intent(in) :: hx, hy, hz
      type(comms_linear),intent(inout) :: collcom_sr
      type(sparse_matrix),intent(in) :: denskern
      type(linmat_auxiliary) :: aux
      type(matrices),intent(in) :: denskern_
      real(kind=8),dimension(ndimrho),intent(out) :: rho
      logical,intent(out) :: rho_negative
      logical,intent(in),optional :: print_results

      ! Local variables
      integer :: ipt, ii, i0, iiorb, jjorb, iorb, jorb, i, j, ierr, ind, ispin, ishift, ishift_mat, iorb_shift, ia, ib
      real(8) :: tt, total_charge, hxh, hyh, hzh, factor, tt1, tt2, rho_neg
      integer,dimension(:),allocatable :: isend_total
      integer,dimension(:),pointer :: moduloarray
      real(kind=8),dimension(:),allocatable :: rho_local
      character(len=*),parameter :: subname='sumrho_for_TMBs'
      logical :: print_local
      integer :: size_of_double, mpisource, istsource, istdest, nsize, jproc, ishift_dest, ishift_source
      !integer :: info

      call f_routine('sumrho_for_TMBs')

      !!call get_modulo_array(denskern%nfvctr, aux%mat_ind_compr, moduloarray)

      ! check whether all entries of the charge density are positive
      rho_negative=.false.

      if (present(print_results)) then
          if (print_results) then
              print_local=.true.
          else
              print_local=.false.
          end if
      else
          print_local=.true.
      end if


      rho_local = f_malloc(collcom_sr%nptsp_c*denskern%nspin,id='rho_local')

      ! Define some constant factors.
      hxh=.5d0*hx
      hyh=.5d0*hy
      hzh=.5d0*hz
      factor=1.d0/(hxh*hyh*hzh)

      call timing(iproc,'sumrho_TMB    ','ON')

      ! Initialize rho. (not necessary for the moment)
      !if (xc_isgga()) then
      !    call f_zero(collcom_sr%nptsp_c, rho_local)
      !else
       !   ! There is no mpi_allreduce, therefore directly initialize to
       !   ! 10^-20 and not 10^-20/nproc.
      !    rho_local=1.d-20
      !end if


      !!if (print_local .and. iproc==0) write(*,'(a)', advance='no') 'Calculating charge density... '

      total_charge=0.d0
      rho_neg=0.d0

    !ispin=1
      !SM: check if the modulo operations take a lot of time. If so, try to use an
      !auxiliary array with shifted bounds in order to access smat%matrixindex_in_compressed_fortransposed
      do ispin=1,denskern%nspin
          if (ispin==1) then
              ishift=0
          else
              ishift=collcom_sr%ndimind_c/2
          end if
          iorb_shift=(ispin-1)*denskern%nfvctr
          ishift_mat=(ispin-1)*denskern%nvctr
          !$omp parallel default(private) &
          !$omp shared(total_charge, collcom_sr, factor, denskern, aux, denskern_, rho_local, moduloarray) &
          !$omp shared(rho_neg, ispin, ishift, ishift_mat, iorb_shift)
          !$omp do schedule(static,200) reduction(+:total_charge, rho_neg)
          do ipt=1,collcom_sr%nptsp_c
              ii=collcom_sr%norb_per_gridpoint_c(ipt)

              i0=collcom_sr%isptsp_c(ipt)+ishift
              tt=1.d-20
              do i=1,ii
                  iiorb=collcom_sr%indexrecvorbital_c(i0+i) - iorb_shift
                  ia = iiorb-aux%mat_ind_compr2(iiorb)%offset_compr
                  ib = sign(1,ia)
                  tt1=collcom_sr%psit_c(i0+i)
                  ind = aux%mat_ind_compr2(iiorb)%section(ib)%ind_compr(iiorb)
                  ind=ind+ishift_mat-denskern%isvctrp_tg
                  tt=tt+denskern_%matrix_compr(ind)*tt1*tt1
                  tt2=2.0d0*tt1
                  do j=i+1,ii
                      jjorb=collcom_sr%indexrecvorbital_c(i0+j) - iorb_shift
                      ia = jjorb-aux%mat_ind_compr2(iiorb)%offset_compr
                      ib = sign(1,ia)
                      !!if (jjorb>ubound(aux%mat_ind_compr2(iiorb)%section(ib)%ind_compr,1)) then
                      !!    write(1000+iproc,*) 'ipt, i, iiorb, offset, jjorb, ia, ib, ubound', &
                      !!         ipt, i, iiorb, aux%mat_ind_compr2(iiorb)%offset_compr, jjorb, ia, ib, &
                      !!         ubound(aux%mat_ind_compr2(iiorb)%section(ib)%ind_compr,1)
                      !!end if
                      ind = aux%mat_ind_compr2(iiorb)%section(ib)%ind_compr(jjorb)
                      if (ind==0) cycle
                      ind=ind+ishift_mat-denskern%isvctrp_tg
                      tt=tt+denskern_%matrix_compr(ind)*tt2*collcom_sr%psit_c(i0+j)
                  end do
              end do
              tt=factor*tt
              total_charge=total_charge+tt
              rho_local(ipt+(ispin-1)*collcom_sr%nptsp_c)=tt
              if (tt<0.d0) rho_neg=rho_neg+1.d0
          end do
          !$omp end do
          !$omp end parallel
      end do

      !call f_free_ptr(moduloarray)

      !if (print_local .and. iproc==0) write(*,'(a)') 'done.'

      call timing(iproc,'sumrho_TMB    ','OF')


      call communicate_density_for_TMBs()


      !!if (nproc > 1) then
      !!   call fmpi_allreduce(irho, 1, FMPI_SUM, bigdft_mpi%mpi_comm)
      !!end if

      if (rho_neg>0.d0) then
          rho_negative=.true.
      end if

      call f_free(rho_local)

      call f_release_routine()

      contains

        subroutine communicate_density_for_TMBs()
          use module_bigdft_profiling
          implicit none
          real(kind=8),dimension(2) :: reducearr

          call f_routine(id='communicate_density')
          call timing(iproc,'sumrho_allred','ON')

          ! Communicate the density to meet the shape required by the Poisson solver.
          !!if (nproc>1) then
          !!    call mpi_alltoallv(rho_local, collcom_sr%nsendcounts_repartitionrho, collcom_sr%nsenddspls_repartitionrho, &
          !!                       mpi_double_precision, rho, collcom_sr%nrecvcounts_repartitionrho, &
          !!                       collcom_sr%nrecvdspls_repartitionrho, mpi_double_precision, bigdft_mpi%mpi_comm, ierr)
          !!else
          !!    call vcopy(ndimrho, rho_local(1), 1, rho(1), 1)
          !!end if

          !!!!do ierr=1,size(rho)
          !!!!    write(200+iproc,*) ierr, rho(ierr)
          !!!!end do



          if (nproc>1) then
              !!call mpi_type_size(mpi_double_precision, size_of_double, ierr)
              !!call mpi_info_create(info, ierr)
              !!call mpi_info_set(info, "no_locks", "true", ierr)
              !!call mpi_win_create(rho_local(1), int(collcom_sr%nptsp_c*denskern%nspin*size_of_double,kind=mpi_address_kind), &
              !!     size_of_double, info, bigdft_mpi%mpi_comm, collcom_sr%window, ierr)
              !!call mpi_info_free(info, ierr)
              !!call mpi_win_fence(mpi_mode_noprecede, collcom_sr%window, ierr)
!              call mpi_type_size(mpi_double_precision, size_of_double, ierr)

              ! This is a bit quick and dirty. Could be done in a better way, but
              ! would probably required to pass additional arguments to the subroutine
              isend_total = f_malloc0(0.to.nproc-1,id='isend_total')
              isend_total(iproc)=collcom_sr%nptsp_c
              call fmpi_allreduce(isend_total, FMPI_SUM, comm=bigdft_mpi%mpi_comm)


              !collcom_sr%window = mpiwindow(collcom_sr%nptsp_c*denskern%nspin, rho_local(1), bigdft_mpi%mpi_comm)
              call fmpi_win_create(collcom_sr%window, rho_local(1), collcom_sr%nptsp_c*denskern%nspin, bigdft_mpi%mpi_comm)
              call fmpi_win_fence(collcom_sr%window,FMPI_WIN_OPEN)

              do ispin=1,denskern%nspin
                  !ishift_dest=(ispin-1)*sum(collcom_sr%commarr_repartitionrho(4,:)) !spin shift for the receive buffer
                  ishift_dest=(ispin-1)*ndimrho/denskern%nspin
                  do jproc=1,collcom_sr%ncomms_repartitionrho
                      mpisource=collcom_sr%commarr_repartitionrho(1,jproc)
                      istsource=collcom_sr%commarr_repartitionrho(2,jproc)
                      istdest=collcom_sr%commarr_repartitionrho(3,jproc)
                      nsize=collcom_sr%commarr_repartitionrho(4,jproc)
                      ishift_source=(ispin-1)*isend_total(mpisource) !spin shift for the send buffer
                      if (nsize>0) then
                          !!write(*,'(6(a,i0))') 'process ',iproc, ' gets ',nsize,' elements at position ',istdest+ishift_dest, &
                          !!                     ' from position ',istsource+ishift_source,' on process ',mpisource, &
                          !!                     '; error code=',ierr
                          call fmpi_get(rho,mpisource,collcom_sr%window, nsize, &
                               int((istsource-1+ishift_source),kind=fmpi_address),&
                               origin_displ=istdest-1+ishift_dest)
                          ! call mpi_get(rho(istdest+ishift_dest), nsize, mpi_double_precision, mpisource, &
                          !      int((istsource-1+ishift_source),kind=mpi_address_kind), &
                          !      nsize, mpi_double_precision, collcom_sr%window, ierr)
                      end if
                  end do
              end do
              !!call mpi_win_fence(0, collcom_sr%window, ierr)
              !!call mpi_win_free(collcom_sr%window, ierr)
              !call mpi_fenceandfree(collcom_sr%window)
              call fmpi_win_shut(collcom_sr%window)

              call f_free(isend_total)
          else
              call vcopy(ndimrho, rho_local(1), 1, rho(1), 1)
          end if

          !do ierr=1,size(rho)
          !    write(300+iproc,*) ierr, rho(ierr)
          !end do
          !call mpi_finalize(ierr)
          !stop

          if (nproc > 1) then
              reducearr(1) = total_charge
              reducearr(2) = rho_neg
              call fmpi_allreduce(reducearr, FMPI_SUM, comm=bigdft_mpi%mpi_comm)
              total_charge = reducearr(1)
              rho_neg = reducearr(2)
             !call fmpi_allreduce(total_charge, 1, FMPI_SUM, bigdft_mpi%mpi_comm)
          end if

          !!if(print_local .and. iproc==0) write(*,'(3x,a,es20.12)') 'Calculation finished. TOTAL CHARGE = ', total_charge*hxh*hyh*hzh
          if (iproc==0 .and. print_local) then
              call yaml_map('Total charge',total_charge*hxh*hyh*hzh,fmt='(es20.12)')
          end if

          call timing(iproc,'sumrho_allred','OF')
          call f_release_routine()

        end subroutine communicate_density_for_TMBs


        !function get_transposed_index(jorb,iorb) result(ind)
        !    integer,intent(in) :: jorb, iorb
        !    integer :: ind
        !    integer :: jjorb,iiorb
        !    ! If iorb is smaller than the offset, add a periodic shift
        !    if (iorb<smat%offset_matrixindex_in_compressed_fortransposed) then
        !        iiorb = iorb + smat%nfvctr
        !    else
        !        iiorb = iorb
        !    end if
        !    if (jorb<smat%offset_matrixindex_in_compressed_fortransposed) then
        !        jjorb = jorb + smat%nfvctr
        !    else
        !        jjorb = jorb
        !    end if
        !    ind = smat%matrixindex_in_compressed_fortransposed(jjorb,iiorb)
        !end function get_transposed_index

      !!write(*,*) 'after deallocate'
      !!call mpi_finalize(ierr)
      !!stop


    end subroutine sumrho_for_TMBs


    subroutine corrections_for_negative_charge(iproc, nproc, at, denspot)
      use module_types
      use yaml_output
      use module_bigdft_profiling
      implicit none

      ! Calling arguments
      integer, intent(in) :: iproc, nproc
      type(atoms_data), intent(in) :: at
      type(DFT_local_fields), intent(inout) :: denspot

      call f_routine(id='corrections_for_negative_charge')

      if (iproc==0) call yaml_warning('No increase of FOE cutoff')
      call clean_rho(iproc, nproc, denspot%dpbox%ndimrhopot, denspot%rhov)

      call f_release_routine()

    end subroutine corrections_for_negative_charge


    !> Set negative entries to zero
    subroutine clean_rho(iproc, nproc, npt, rho)
      use yaml_output
      use module_bigdft_mpi
      implicit none

      ! Calling arguments
      integer, intent(in) :: iproc, nproc, npt
      real(kind=8),dimension(npt), intent(inout) :: rho

      ! Local variables
      integer :: ncorrection, ipt
      real(kind=8) :: charge_correction

      if (iproc==0) then
          call yaml_newline()
          call yaml_map('Need to correct charge density',.true.)
          call yaml_warning('set to 1.d-20 instead of 0.d0')
      end if

      ncorrection=0
      charge_correction=0.d0
      do ipt=1,npt
          if (rho(ipt)<0.d0) then
              if (rho(ipt)>=-1.d-5) then
                  ! negative, but small, so simply set to zero
                  charge_correction=charge_correction+rho(ipt)
                  !rho(ipt)=0.d0
                  rho(ipt)=1.d-20
                  ncorrection=ncorrection+1
              else
                  ! negative, but non-negligible, so issue a warning
                  ! only print first time this occurs
                  if (ncorrection==0) then
                      call yaml_warning('considerable negative rho, value: '//&
                        &trim(yaml_toa(rho(ipt),fmt='(es12.4)')))
                  end if
                  charge_correction=charge_correction+rho(ipt)
                  !rho(ipt)=0.d0
                  rho(ipt)=1.d-20
                  ncorrection=ncorrection+1
              end if
          end if
      end do

      if (nproc > 1) then
          call fmpi_allreduce(ncorrection, 1, FMPI_SUM, comm=bigdft_mpi%mpi_comm)
          call fmpi_allreduce(charge_correction, 1, FMPI_SUM, comm=bigdft_mpi%mpi_comm)
      end if

      if (iproc==0) then
          call yaml_newline()
          call yaml_map('number of corrected points',ncorrection)
          call yaml_newline()
          call yaml_map('total charge correction',abs(charge_correction),fmt='(es14.5)')
          call yaml_newline()
      end if

    end subroutine clean_rho


    subroutine extract_potential(ispin, lr, glr, pot, comgp)
      use module_types
      use locregs
      use locreg_operations, only: global_to_local_parallel
      implicit none

      ! Calling arguments
      integer,intent(in) :: ispin
      type(locreg_descriptors), intent(in) :: lr, glr
      real(kind=8),dimension(lr%mesh%ndim),intent(out) :: pot
      type(p2pComms),intent(in):: comgp

      ! Local variables
      integer :: ishift, i1s, i1e, i2s, i2e, i3s, i3e, ni1, ni2, ni3, i1shift, i2shift, i3shift

      ! spin shift of the potential in the receive buffer
      ishift=(ispin-1)*comgp%nrecvBuf

      ! Extract the part of the potential which is needed for the current localization region.
      i3s=modulo(lr%nboxi(1,3)-1,glr%mesh%ndims(3))+1-comgp%ise(5)+2 ! starting index of localized  potential with respect to total potential in comgp%recvBuf
      i3e=i3s+lr%mesh%ndims(3)-1 ! ending index of localized potential with respect to total potential in comgp%recvBuf
      i2s=modulo(lr%nboxi(1,2)-1,glr%mesh%ndims(2))+1-comgp%ise(3)+2
      i2e=i2s+lr%mesh%ndims(2)-1
      i1s=modulo(lr%nboxi(1,1)-1,glr%mesh%ndims(1))+1-comgp%ise(1)+2
      i1e=i1s+lr%mesh%ndims(1)-1
      !!!! Make sure that the length is not larger than the global box (only possible for non-free BC)
      if (comgp%ise(2)>=comgp%ise(1)) then
          ni1=comgp%ise(2)-comgp%ise(1)+1
      else
          ! This is the case with aperiodic wrap around
          ni1=comgp%ise(2)+glr%mesh%ndims(1)-comgp%ise(1)+1
      end if
      if (comgp%ise(4)>=comgp%ise(3)) then
          ni2=comgp%ise(4)-comgp%ise(3)+1
      else
          ! This is the case with aperiodic wrap around
          ni2=comgp%ise(4)+glr%mesh%ndims(2)-comgp%ise(3)+1
      end if
      if(i3e-i3s+1 /= lr%mesh%ndims(3)) then
         write(*,'(a,i0,3x,i0)') 'ERROR: i3e-i3s+1 /= lr%mesh%ndims(3)',i3e-i3s+1, lr%mesh%ndims(3)
         stop
      end if

      if (comgp%ise(6)<comgp%ise(5)) then
          i3shift=comgp%ise(6)
      else
          i3shift=0
      end if
      if (comgp%ise(4)<comgp%ise(3)) then
          i2shift=comgp%ise(4)
      else
          i2shift=0
      end if
      if (comgp%ise(2)<comgp%ise(1)) then
          i1shift=comgp%ise(2)
      else
          i1shift=0
      end if
      call global_to_local_parallel(glr, lr, comgp%nrecvBuf, int(lr%mesh%ndim),&
           comgp%recvBuf(ishift+1:), pot, i1s, i1e, i2s, i2e, i3s, i3e, ni1, ni2, &
           i1shift, i2shift, i3shift, comgp%ise)

     end subroutine extract_potential

     subroutine exchange_and_correlation(xcObj,dpbox,&
          rho,exc,vxc,nspin,rhocore,rhohat,potxc,xcstr,dvxcdrho)
       use module_dpbox
       use module_xc
       use module_precisions
       use module_bigdft_arrays
       implicit none
       integer, intent(in) :: nspin !< Value of the spin-polarisation
       type(xc_info), intent(in) :: xcObj
       type(denspot_distribution), intent(in) :: dpbox !<descriptors of the potential and density distribution
       real(gp), intent(out) :: exc,vxc
       !>on input, the components of the electronic density (either collinear or spinorial).
       !! on output, the first component is overwritten with the charge density that have to
       !! be passed to the Poisson Solver for the Hartree potential.
       real(dp), dimension(dpbox%ndimrho,nspin), intent(inout) :: rho
       real(wp), dimension(:,:,:,:), pointer :: rhocore !associated if useful
       real(wp), dimension(:,:,:,:), pointer :: rhohat !associated if useful
       real(wp), dimension(dpbox%ndimpot,nspin), intent(out) :: potxc
       real(dp), dimension(6), intent(out) :: xcstr
       real(dp), dimension(:,:,:,:), target, intent(out), optional :: dvxcdrho
       !local variables
       real(dp), dimension(:), allocatable :: m_norm
       real(dp), dimension(:,:), allocatable :: rho_diag

       if (nspin==4) then
          rho_diag = f_malloc([dpbox%ndimrho, 2],id='rho_diag')
          m_norm = f_malloc(dpbox%ndimrho,id='m_norm')
          !           print *,'Rho Dims',shape(rhopot),shape(rho_diag)
          !rho to rho_diag
          call get_local_magnetization(dpbox%ndimrho,rho,m_norm,rho_diag)

          call exchange_and_correlation_collinear(xcObj,dpbox,&
               rho_diag,exc,vxc,2,rhocore,rhohat,potxc,xcstr,dvxcdrho)
          !comment out this term for the implementation of the (presumably incorrect) previous version
          call get_spinorial_potential(dpbox%ndimpot,potxc,m_norm,rho)

          call f_memcpy(src=rho,dest=potxc)
          !copy the charge density in the first components of rho
          call f_memcpy(n=dpbox%ndimpot,src=rho_diag(1,1),dest=rho(1,1))


          call f_free(rho_diag)
          call f_free(m_norm)
       else
          call exchange_and_correlation_collinear(xcObj,dpbox,&
               rho,exc,vxc,nspin,rhocore,rhohat,potxc,xcstr,dvxcdrho)
       end if

     end subroutine exchange_and_correlation


     subroutine exchange_and_correlation_collinear(xcObj,dpbox,&
          rho,exc,vxc,nspin,rhocore,rhohat,potxc,xcstr,dvxcdrho)
       use module_precisions
       use box
       use at_domain, only: domain_geocode
       use module_dpbox
       use Poisson_Solver, except_dp => dp, except_gp => gp
       !Idem
       use module_interfaces, only: calc_gradient
       use module_xc
       use module_bigdft_profiling
       use abi_interfaces_xc_lowlevel, only: abi_mkdenpos
       use f_ternary
       use module_bigdft_mpi
       use module_bigdft_profiling
       use wrapper_linalg
       use module_bigdft_arrays
       implicit none
       integer, intent(in) :: nspin !< Value of the spin-polarisation
       type(xc_info), intent(in) :: xcObj
       type(denspot_distribution), intent(in) :: dpbox !<descriptors of the potential and density distribution
       real(gp), intent(out) :: exc,vxc
       !>on input, the components of the electronic density (either collinear or spinorial).
       !! on output, the first component is overwritten with the charge density that have to
       !! be passed to the Poisson Solver for the Hartree potential.
       real(dp), dimension(dpbox%ndimrho,nspin), intent(inout) :: rho
       real(wp), dimension(:,:,:,:), pointer :: rhocore !associated if useful
       real(wp), dimension(:,:,:,:), pointer :: rhohat !associated if useful
       real(wp), dimension(dpbox%ndimpot,nspin), intent(out) :: potxc
       real(dp), dimension(6), intent(out) :: xcstr
       real(dp), dimension(:,:,:,:), target, intent(out), optional :: dvxcdrho
       !local variables
       integer :: ierr,i,j,i3s_fake,i3xcsh_fake
       integer :: i1,i2,i3,iwarn,i3start,jend,jproc
       integer :: nxc,nwbl,nwbr,nxt,nwb,nxcl,nxcr,ispin,istden,istglo
       integer :: ndvxc,order
       real(dp) :: eexcuLOC,vexcuLOC,vexcuRC
       real(dp), dimension(6) :: wbstr, rhocstr
       real(dp), dimension(:,:,:,:,:), allocatable :: gradient
       real(dp), dimension(:,:,:,:), allocatable :: vxci
       real(gp), dimension(:), allocatable :: energies_mpi
       real(dp), dimension(:,:,:,:), pointer :: dvxci

       call f_routine(id='exchange_and_correlation')
       call f_timing(TCAT_EXCHANGECORR,'ON')

       call f_zero(xcstr)
       call f_zero(wbstr)
       call f_zero(rhocstr)

       !quick return if no Semilocal XC potential is required (Hartree or Hartree-Fock)
       if (any(xcObj%ixc == [XC_HARTREE,XC_HARTREE_FOCK,XC_NO_HARTREE])) then
          call f_zero(potxc)
          !here construct the charge density from any spin value
          if (nspin == 2) call axpy(dpbox%ndimrho,1.d0,rho(1,2),1,rho(1,1),1)
          exc=0.0_gp
          vxc=0.0_gp
          call f_timing(TCAT_EXCHANGECORR,'OF')
          call f_release_routine()
          return
       end if

       !here we can transform the charge density into majority and minority components
       !we may therefore end up with rho_diag

       if (dpbox%n3p==0) then
          nwbl=0
          nwbr=0
          nxcl=1
          nxcr=1
          nxc=0
       else
          !here istart and iend are provided as input variables
          call xc_dimensions(domain_geocode(dpbox%mesh%dom),xc_isgga(xcObj),xcObj%ixc/=13,&
               dpbox%i3s+dpbox%i3xcsh-1,dpbox%i3s+dpbox%i3xcsh-1+dpbox%n3p,&
               dpbox%mesh%ndims(3),nxc,nxcl,nxcr,nwbl,nwbr,i3s_fake,i3xcsh_fake)
       end if
       nwb=nxcl+nxc+nxcr-2
       nxt=nwbr+nwb+nwbl

!!$ ! !if rhocore is associated we should add it on the charge density
!!$ ! if (associated(rhocore)) then
!!$ !    if (nspin == 1) then
!!$ !       !sum the complete core density for non-spin polarised calculations
!!$ !       call axpy(m1*m3*nxt,1.0_wp,rhocore(1,1,1,1),1,rho(1),1)
!!$ !    else if (nspin==2) then
!!$ !       !for spin-polarised calculation consider half per spin index
!!$ !       call axpy(m1*m3*nxt,0.5_wp,rhocore(1,1,1,1),1,rho(1),1)
!!$ !       call axpy(m1*m3*nxt,0.5_wp,rhocore(1,1,1,1),1,rho(1+m1*m3*nxt),1)
!!$ !    end if
!!$ ! end if


       !if rhohat is present, substract it from charge density
       if (associated(rhohat) .and. dpbox%ndimpot > 0) then
          call axpy(dpbox%ndimpot,-1._wp,rhohat(1,1,1,1),1,&
               rho(1+dpbox%mesh%ndims(1)*dpbox%mesh%ndims(2)*dpbox%i3xcsh,1),1)
          if (nspin==2) call axpy(dpbox%ndimpot,-1._wp,rhohat(1,1,1,2),1,&
               rho(1+dpbox%mesh%ndims(1)*dpbox%mesh%ndims(2)*dpbox%i3xcsh,2),1)
       end if

       !rescale the density to apply that to ABINIT routines
       if (nspin==1 .and. dpbox%ndimrho > 0) call vscal(dpbox%ndimrho,0.5_dp,rho(1,1),1)

       !allocate array for XC potential enlarged for the WB procedure
       vxci = f_malloc0([ dpbox%mesh%ndims(1), dpbox%mesh%ndims(2), max(1, nwb), nspin],id='vxci')

       !allocate the array of the second derivative of the XC energy if it is needed
       !Allocations of the exchange-correlation terms, depending on the ixc value
       if (present(dvxcdrho)) then
          !if (nspin==1) then
          order=.if. (nspin==1) .then. -2 .else. 2
          !else
          !   order=2
          !end if
          ndvxc = size(dvxcdrho, dim=4)
          dvxci => dvxcdrho
       else
          order=1
          ndvxc=0
          dvxci = f_malloc_ptr([dpbox%mesh%ndims(1), dpbox%mesh%ndims(2), max(1, nwb), ndvxc],id='dvxci')
       end if

       !calculate gradient
       if (xc_isgga(xcObj) .and. nxc > 0) then
          !computation of the gradient
          gradient = &
               f_malloc([1.to.dpbox%mesh%ndims(1), 1.to.dpbox%mesh%ndims(2), 1.to.nwb, 1.to.2*nspin-1, 0.to.3],id='gradient')

          !!the calculation of the gradient will depend on the geometry code
          !this operation will also modify the density arrangment for a GGA calculation
          !in parallel and spin-polarised, since ABINIT routines need to calculate
          !the XC terms for spin up and then spin down
          !part which have to be modified for non-orthorhombic cells
          call calc_gradient(domain_geocode(dpbox%mesh%dom),dpbox%mesh%ndims(1), dpbox%mesh%ndims(2),&
               nxt,nwb,nwbl,nwbr,rho(1,1),nspin,&
               dpbox%mesh%hgrids(1),dpbox%mesh%hgrids(2),dpbox%mesh%hgrids(3),gradient,rhocore)
       else
          gradient = f_malloc((/ 1, 1, 1, 1, 1 /),id='gradient')
          !add rhocore to the density
          if (associated(rhocore) .and. dpbox%ndimrho > 0) then
             call axpy(dpbox%ndimrho,0.5_wp,rhocore(1,1,1,1),1,rho(1,1),1)
             if (nspin==2) call axpy(dpbox%ndimrho,0.5_wp,rhocore(1,1,1,1),1,&
                  rho(1,2),1)
          end if
       end if

       if (dpbox%n3p>0) then
          call xc_energy_new(domain_geocode(dpbox%mesh%dom),dpbox%mesh%ndims(1), dpbox%mesh%ndims(2),&
               nxc,nwb,nxt,nwbl,nwbr,nxcl,nxcr,&
               xcObj,dpbox%mesh%hgrids(1),dpbox%mesh%hgrids(2),dpbox%mesh%hgrids(3),&
               rho(1,1),gradient,vxci,&
               eexcuLOC,vexcuLOC,order,ndvxc,dvxci,nspin,wbstr)
       else
          !presumably the vxc should be initialised
          eexcuLOC=0.0_dp
          vexcuLOC=0.0_dp
       end if

       !deallocate gradient here
       call f_free(gradient)

       !the value of the shift depends of the distributed i/o or not
       !copy the relevant part of vxci on the output potxc
       if (dpbox%ndimpot > 0) then
          call vcopy(dpbox%ndimpot,vxci(1,1,nxcl,1),1,potxc(1,1),1)
          if (nspin == 2) then
             call vcopy(dpbox%ndimpot,vxci(1,1,nxcl,2),1,potxc(1,2),1)
          end if
       end if

       !if rhohat is present, add it to charge density
       if(associated(rhohat) .and. dpbox%ndimpot .gt.0) then
          call axpy(dpbox%ndimpot,1._wp,rhohat(1,1,1,1),1,rho(1,1),1)
          if (nspin==2) call axpy(dpbox%ndimpot,1._wp,rhohat(1,1,1,2),1,&
               rho(1,2),1)
          !This will add V_xc(n) nhat to V_xc(n) n, to get: V_xc(n) (n+nhat)
          ! Only if usexcnhat /= 0, not the default case.
          !call add_to_vexcu(rhohat)
       end if

       !if rhocore is associated we then remove it from the charge density
       !and subtract its contribution from the evaluation of the XC potential integral vexcu
       if (associated(rhocore) .and. dpbox%ndimpot .gt.0) then
          !at this stage the density is not anymore spin-polarised
          !sum the complete core density for non-spin polarised calculations
          call axpy(dpbox%ndimpot,-1.0_wp,rhocore(1,1,dpbox%i3xcsh+1,1),1,rho(1,1),1)
          call substract_from_vexcu(rhocore)
          !print *,' aaaa', vexcuRC,vexcuLOC,eexcuLOC
       end if

       !gathering the data to obtain the distribution array
       !evaluating the total ehartree,eexcu,vexcu
       if (bigdft_mpi%nproc > 1) then
          energies_mpi = f_malloc(4,id='energies_mpi')

          energies_mpi(1)=eexcuLOC
          energies_mpi(2)=vexcuLOC
          call fmpi_allreduce(energies_mpi(1), 2,FMPI_SUM,comm=bigdft_mpi%mpi_comm,recvbuf=energies_mpi(3))
          exc=energies_mpi(3)
          vxc=energies_mpi(4)

          call f_free(energies_mpi)

       else
          exc=real(eexcuLOC,gp)
          vxc=real(vexcuLOC,gp)
       end if

       !XC-stress term
       if (domain_geocode(dpbox%mesh%dom) == 'P') then
          if (associated(rhocore)) then
             call calc_rhocstr(rhocstr,nxc,nxt,dpbox%mesh%ndims(1), dpbox%mesh%ndims(2),&
                  dpbox%i3xcsh,nspin,potxc,rhocore)
             if (bigdft_mpi%nproc > 1) call fmpi_allreduce(rhocstr,FMPI_SUM,comm=bigdft_mpi%mpi_comm)
             rhocstr=rhocstr/real(dpbox%mesh%ndim,dp)
          end if

          xcstr(1:3)=(exc-vxc)/real(dpbox%mesh%ndim,dp)/dpbox%mesh%volume_element
          if (bigdft_mpi%nproc > 1) call fmpi_allreduce(wbstr,FMPI_SUM,comm=bigdft_mpi%mpi_comm)
          wbstr=wbstr/real(dpbox%mesh%ndim,dp)
          xcstr=xcstr+wbstr+rhocstr
       end if

       call f_free(vxci)

       if (.not.present(dvxcdrho)) call f_free_ptr(dvxci)

       call f_timing(TCAT_EXCHANGECORR,'OF')
       call f_release_routine()

     contains
       subroutine substract_from_vexcu(rhoin)
         implicit none
         real(wp),dimension(:,:,:,:),intent(in)::rhoin

!!$ vexcuRC=0.0_gp
!!$ do i3=1,nxc
!!$    do i2=1,m3
!!$       do i1=1,m1
!!$          !do i=1,nxc*m3*m1
!!$          i=i1+(i2-1)*m1+(i3-1)*m1*m3
!!$          vexcuRC=vexcuRC+rhoin(i1,i2,i3+dpbox%i3xcsh,1)*potxc(i,1)
!!$       end do
!!$    end do
!!$ end do
!!$ if (nspin==2) then
!!$    do i3=1,nxc
!!$       do i2=1,m3
!!$          do i1=1,m1
!!$             !do i=1,nxc*m3*m1
!!$             !vexcuRC=vexcuRC+rhocore(i+m1*m3*dpbox%i3xcsh)*potxc(i+dpbox%ndimpot)
!!$             i=i1+(i2-1)*m1+(i3-1)*m1*m3
!!$             vexcuRC=vexcuRC+rhoin(i1,i2,i3+dpbox%i3xcsh,1)*potxc(i,2)
!!$          end do
!!$       end do
!!$    end do
!!$    !divide the results per two because of the spin multiplicity
!!$    vexcuRC=0.5*vexcuRC
!!$ end if
         vexcuRC=dot(dpbox%ndimpot,rhoin(1,1,1+dpbox%i3xcsh,1),1,potxc(1,1),1)
         if (nspin==2) then
            vexcuRC=vexcuRC+dot(dpbox%ndimpot,rhoin(1,1,1+dpbox%i3xcsh,1),1,potxc(1,2),1)
            !divide the results per two because of the spin multiplicity
            vexcuRC=0.5_wp*vexcuRC
         end if
         vexcuRC=vexcuRC*dpbox%mesh%volume_element
         !subtract this value from the vexcu
         vexcuLOC=vexcuLOC-vexcuRC

       end subroutine substract_from_vexcu

     END SUBROUTINE exchange_and_correlation_collinear

     subroutine set_cfd_data(cfd,mesh,astruct,rxyz)
       use module_precisions
       use module_cfd
       use box
       use module_atoms
       use module_bigdft_arrays
       use yaml_output
       use at_domain, only: distance
       implicit none
       type(atomic_structure), intent(in) :: astruct
       type(cell), intent(in) :: mesh
       real(gp), dimension(3,astruct%nat), intent(in) :: rxyz
       type(cfd_data), intent(out) :: cfd
       !local variables
       integer :: jat
       real(gp) :: r
       type(atoms_iterator) :: atit
       type(atomic_neighbours) :: nnit
       character*30 :: external_fname

       !fill the positions
       call cfd_allocate(cfd,astruct%nat)

       call cfd_set_centers(cfd,rxyz)

       !read in reference moments (to be changed later) AB
       external_fname='moments'
       call cfd_read_external(cfd,external_fname)

       !adjust the radii with the nn iterator
       !iterate above atoms
       atit=atoms_iter(astruct)
       !iterate of nearest neighbors
       call astruct_neighbours(astruct, rxyz, nnit)
       loop_at: do while(atoms_iter_next(atit))
          call astruct_neighbours_iter(nnit, atit%iat)
          do while(astruct_neighbours_next(nnit, jat))
             !set the radius as the distance bw the atom iat and its nearest
             r=distance(mesh%dom,rxyz(1,atit%iat),rxyz(1,jat))
             call cfd_set_radius(cfd,atit%iat,0.5_gp*r)
             cycle loop_at
          end do
       end do loop_at
       call deallocate_atomic_neighbours(nnit)
     end subroutine set_cfd_data


end module rhopotential

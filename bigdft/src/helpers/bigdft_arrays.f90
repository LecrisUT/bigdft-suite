module module_bigdft_arrays
  use dynamic_memory, only: f_malloc, f_free, f_malloc_ptr, f_free_ptr, &
       f_malloc0, f_malloc0_ptr, operator(.to.), assignment(=), &
       f_memcpy, f_subptr, f_malloc_str_ptr, f_malloc0_str, f_free_str, &
       f_free_str_ptr, f_malloc0_str_ptr, f_malloc_set_status, &
       f_update_database, f_malloc_str, f_purge_database
  use wrapper_linalg, only: vcopy
  use f_utils, only: f_zero
  
  implicit none

  public
end module module_bigdft_arrays

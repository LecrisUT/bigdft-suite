<?xml version="1.0"?><!--*- mode: nxml; indent-tabs-mode: nil -*-->
<!DOCTYPE moduleset SYSTEM "moduleset.dtd">
<?xml-stylesheet type="text/xsl" href="moduleset.xsl"?>
<!-- vim:set ts=2 expandtab: -->
<moduleset>

  <repository type="tarball" name="gsl" href="http://mirrors.kernel.org/gnu/gsl/"/>
  <repository type="tarball" name="hdf5" href="https://support.hdfgroup.org/ftp/HDF5/current/src/"/>
  <repository type="tarball" name="libxc" href="https://gitlab.com/libxc/libxc/-/archive/4.3.4/"/>
  <repository type="tarball" name="spglib-dev" href="https://github.com/spglib/spglib/archive/refs/tags/"/>
  <repository type="tarball" name="spglib-python" href="./spglib-dev/python/"/>
  <repository type="tarball" name="fftw" href="http://www.fftw.org/"/>
  <repository type="tarball" name="ntpoly" href="https://github.com/william-dawson/NTPoly/archive/"/>
  <repository type="tarball" name="dbfft" href="https://github.com/intel/double-batched-fft-library/archive/refs/tags/"/>
  <repository type="tarball" name="levelzero" href="https://github.com/oneapi-src/level-zero/archive/refs/tags/"/>
  <repository type="tarball" name="intelruntime" href="https://github.com/intel/compute-runtime/archive/refs/tags/"/>
  <repository type="tarball" name="intelgraphicscompiler" href="https://github.com/intel/intel-graphics-compiler/archive/refs/tags/"/>
  <repository type="tarball" name="llvm" href="https://github.com/llvm/llvm-project/releases/download/llvmorg-16.0.6/"/>
  <repository type="tarball" name="spirv" href="https://github.com/KhronosGroup/SPIRV-LLVM-Translator/archive/refs/tags/"/>
  <repository type="tarball" name="opencl-clang" href="https://github.com/intel/opencl-clang/archive/refs/tags/"/>
  

  <autotools id="libxc" check-target="false" autogen-sh="autoreconf">
    <branch repo="libxc" module="libxc-4.3.4.tar.gz" version="4.3.4" checkoutdir="libxc">
    </branch>
  </autotools>

  <cmake id="ntpoly" use-ninja="no">
    <branch repo="ntpoly" module="ntpoly-v3.0.0.tar.gz" version="3.0.0" checkoutdir="ntpoly"/>
    <cmakeargs value="-DFORTRAN_ONLY=Yes"/>
  </cmake>

  <cmake id="dbfft" use-ninja="no">
     <branch repo="dbfft"  module="v0.3.6.tar.gz" version="0.3.6" checkoutdir="dbfft"/>
     <dependencies>
      <if condition-set="no_intel_gpu">
         <dep package="levelzero"/>
         <!-- <dep package="intelruntime"/> -->
      </if>
    </dependencies>
  </cmake>

  <cmake id="levelzero" use-ninja="no">
     <branch repo="levelzero"  module="v1.13.1.tar.gz" version="1.13.1" checkoutdir="levelzero"/>
  </cmake>

  <cmake id="intelruntime" use-ninja="no">
     <branch repo="intelruntime"  module="23.22.26516.18.tar.gz" version="23.22.26516.18" checkoutdir="intelruntime"/>
     <dependencies>
       <dep package="igc"/>
    </dependencies>          
  </cmake>

  <cmake id="igc" use-ninja="no">
     <branch repo="intelgraphicscompiler"  module="igc-1.0.14062.11.tar.gz" version="1.0.14062.11" checkoutdir="igc"/>
     <cmakeargs value="-DIGC_OPTION__LLVM_PREFERRED_VERSION=16.0.6 -DIGC_OPTION__SPIRV_TOOLS_MODE=Prebuilds -DIGC_OPTION__USE_PREINSTALLED_SPIRV_HEADERS=ON"/>
     <dependencies>
       <dep package="llvm"/>
       <dep package="spirv"/>
       <dep package="opencl-clang"/>
      </dependencies>               
  </cmake>

  <cmake id="llvm" use-ninja="no" cmakedir="llvm">
     <branch repo="llvm"  module="llvm-project-16.0.6.src.tar.xz" version="16.0.6" checkoutdir="llvm"/>
    <cmakeargs value="-DCMAKE_BUILD_TYPE=Release"/>
  </cmake>

  <cmake id="spirv" use-ninja="no">
     <branch repo="spirv"  module="v16.0.0.tar.gz" version="16.0.0" checkoutdir="spirv"/>
  </cmake>

  <cmake id="opencl-clang" use-ninja="no">
     <branch repo="opencl-clang"  module="v16.0.0.tar.gz" version="16.0.0" checkoutdir="opencl-clang"/>
  </cmake>



  <cmake id="spglib-dev" use-ninja="no">
     <branch repo="spglib-dev"  module="v1.11.2.1.tar.gz" version="1.11.2.1" checkoutdir="spglib-dev"/>
  </cmake>

  <distutils id="pyspglib" supports-non-srcdir-builds='no' >
    <branch repo="spglib-python" module="v1.14.2.tar.gz"
            version="1.14.2" checkoutdir="spglib-dev/python"/>
    <dependencies>
      <dep package="spglib-dev"/>
    </dependencies>
  </distutils>

  <cmake id="hdf5" use-ninja="no">
    <branch repo="hdf5" module="hdf5-1.10.5.tar.bz2"
            version="1.10"/>
  </cmake>

  <autotools id="gsl" autogen-sh="autoreconf">
    <branch repo="gsl" module="gsl-2.6.tar.gz"
            version="2.6"/>
  </autotools>

  <autotools id="fftw" autogen-sh="autoreconf">
    <branch repo="fftw" module="fftw-3.3.8.tar.gz" version="3.3.8"
            checkoutdir="fftw">
    </branch>
  </autotools>


</moduleset>

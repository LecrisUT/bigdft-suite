#ifndef REDUCTION_GENERATOR_H
#define REDUCTION_GENERATOR_H

#include "liborbs_ocl.h"

#ifdef __cplusplus
extern "C" char* generate_reduction_program(struct liborbs_device_infos * infos);
#else
char* generate_reduction_program(struct liborbs_device_infos * infos);
#endif

#endif

!> @file
!! Include fortran file for subpointer association

!! @author
!!    Copyright (C) 2022-2022 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS
type(array_bounds), intent(in), optional :: region
!local variables
integer(f_kind) :: lb,ub,is,ie

nullify(win)
call subpointer_bounds(is,ie,lb,ub,size,from,lbound,region)
if (ub < lb) return

!perform the association on the window
if (lb==1) then
   win => ptr(is:ie)
else
   call f_map_ptr(lb,ub,ptr(is:ie),win)
end if

!then perform the check for the subpointer region
if (get_lbnd(win) /= lb) call f_err_throw(&
     'ERROR (f_subptr): expected shape does not match, '//&
     trim(yaml_toa(get_lbnd(win)))//' vs. '//trim(yaml_toa(lb)),&
     ERR_MALLOC_INTERNAL)

if (f_loc(win(lb)) /= f_loc(ptr(is)) .or. &
     f_loc(win(ub)) /= f_loc(ptr(ie))) call f_err_throw(&
     'ERROR (f_subptr): addresses do not match, the allocating system has performed a copy',&
     ERR_MALLOC_INTERNAL)

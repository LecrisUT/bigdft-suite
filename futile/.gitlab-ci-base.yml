.debpackage: &debpackage
  - apt -y install debhelper
  - tar -xf tmp/${CI_PROJECT_NAME}*.tar.bz2
  - cp tmp/${CI_PROJECT_NAME}-*.tar.bz2 $(echo $(ls tmp/${CI_PROJECT_NAME}-*.tar.bz2) | sed "s;tmp/${CI_PROJECT_NAME}-\(.*\).tar.bz2;${CI_PROJECT_NAME}_\1.orig.tar.bz2;")
  - cp -rp debian/ ${CI_PROJECT_NAME}-*
  - cd ${CI_PROJECT_NAME}-*
  - dpkg-buildpackage

.before_doc: &before_doc
  - git clone https://gitlab.com/l_sim/f2py
  - cd f2py
  - $PYTHON setup.py install --prefix=$PWD/usr
  - cd -
  - git clone https://gitlab.com/l_sim/sphinx-fortran
  - cd sphinx-fortran
  - git checkout lsim
  - cd -

.build: &build
  - autoreconf -fi
  - mkdir tmp
  - cd tmp/
  - ../configure FC=mpif90 FCFLAGS="-g -O2 -fbounds-check -fopenmp -I/usr/include" "${CONFIGURE_LINE_EXTRA}"
  - make -j$(nproc)

stages:
  - build
  - package
  - deploy

serial:
  stage: build
  variables:
    PYTHON: python3
  script:
    - *build
    - make dist-bzip2
    - OMP_NUM_THREADS=2 make check
  artifacts:
    when: always
    paths:
      - "tmp/${CI_PROJECT_NAME}*.tar.bz2"

parallel:
  stage: build
  variables:
    PYTHON: python3
  script:
    - *build
    - OMP_NUM_THREADS=2 run_parallel="mpirun -np 3 --allow-run-as-root --oversubscribe" make check

dynamic:
  stage: build
  variables:
    CONFIGURE_LINE_EXTRA: --enable-dynamic-libraries
    PYTHON: python3
  script:
    - *build
    - OMP_NUM_THREADS=2 run_parallel="mpirun -np 3 --allow-run-as-root --oversubscribe" make check


coverage:
  stage: build
  variables:
    PYTHON: python3
  script:
    - apt -y install gcovr
    - autoreconf -fi
    - ./configure FC=mpif90 FCFLAGS="-g -O0 --coverage -fopenmp -I/usr/include"
    - make -j$(nproc)
    - OMP_NUM_THREADS=2 make check
    - gcovr --html -o coverage.html -s
  coverage: '/^lines: +(\d+.\d\%)/'
  artifacts:
    paths:
      - "coverage.html"

sphinx:
  stage: build
  variables:
    PYTHON: python3
  before_script:
    - export DEBIAN_FRONTEND=noninteractive
    - apt update && apt -y install make python3-sphinx python3-sphinx-bootstrap-theme python3-guzzle-sphinx-theme python3-numpy python3-matplotlib python3-distutils git
    - *before_doc
  script:
    - PYTHONPATH=$PWD/f2py/usr/lib/python3.10/site-packages:$PWD/sphinx-fortran sphinx-build -M html doc build  #make -f Makefile-sphinx html
  after_script:
    - mkdir errors
    - cp tmp/* errors
  artifacts:
    when: always
    paths:
      - "errors"
      - "build/html"

debian:
  stage: package
  variables:
    PYTHON: python3
  script:
    - apt -y install nvidia-cuda-toolkit ocl-icd-opencl-dev python3-yaml python3-numpy
    - *debpackage
  artifacts:
    paths:
      - "lib${CI_PROJECT_NAME}*.deb"

.pages:
  stage: deploy
  before_script:
    - echo -n
  script:
    - mv build/html/ ${CI_PROJECT_DIR}/public/
    - mv tmp/${CI_PROJECT_NAME}*.tar.bz2 ${CI_PROJECT_DIR}/public/
    - mv lib${CI_PROJECT_NAME}*.deb ${CI_PROJECT_DIR}/public/
    - mv coverage.html ${CI_PROJECT_DIR}/public/
  artifacts:
    paths:
      - public

pages:
  extends: .pages

!> @file
!!   Basic routines for i/o based on ntpoly.
!! @author
!!   Copyright (C) 2016 CheSS developers
!!
!!   This file is part of CheSS.
!!   
!!   CheSS is free software: you can redistribute it and/or modify
!!   it under the terms of the GNU Lesser General Public License as published by
!!   the Free Software Foundation, either version 3 of the License, or
!!   (at your option) any later version.
!!   
!!   CheSS is distributed in the hope that it will be useful,
!!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!   GNU Lesser General Public License for more details.
!!   
!!   You should have received a copy of the GNU Lesser General Public License
!!   along with CheSS.  If not, see <http://www.gnu.org/licenses/>.


!> Basic routines for i/o based on ntpoly.
module bigpoly_io
  use futile
  use sparsematrix_base, only : sparse_matrix, matrices
  implicit none
  private

   !> Public routines
  public :: bigpoly_read_to_dense
  public :: bigpoly_write_matrix

  contains

    !> Read a matrix from matrix market.
     subroutine bigpoly_read_to_dense(dense, filename, comm, binary)
      !> The dense matrix (preallocated)
      real(8), dimension(:,:) :: dense
      !> The name of the file to read from.
      character(len=*), intent(in) :: filename
      !> The communicator to distribute along.
      integer, intent(in) :: comm
      !> True if it's a binary matrix.
      logical, intent(in) :: binary
      
      call f_err_throw('Not properly linked with ntpoly')
    end subroutine bigpoly_read_to_dense

    !> Write a matrix to matrix market.
    subroutine bigpoly_write_matrix(iproc, nproc, comm, inmat, inval, &
        filename, comm, binary)
      !> Process information
      integer, intent(in) :: iproc, nproc, comm
      !> Input CheSS sparse matrix type
      type(sparse_matrix), intent(in):: inmat
      !> Input CheSS matrix of data.
      type(matrices), intent(in) :: inval
      !> The name of the file to write to.
      character(len=*), intent(in) :: filename
      !> The communicator to distribute along.
      integer, intent(in) :: comm
      !> True if it's a binary matrix.
      logical, intent(in) :: binary

      call f_err_throw('Not properly linked with ntpoly')
    end subroutine bigpoly_write_matrix

end module bigpoly_io

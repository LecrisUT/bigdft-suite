.. BigDFT-suite documentation master file, created by
   sphinx-quickstart on Sun Sep 30 14:58:47 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BigDFT-suite
============

The `BigDFT-suite` project regroups the packages which are needed
to install, run and employ the BigDFT code for production calculations. 
BigDFT is a program for performing Density Functional Theory calculations on a 
wide class of systems. A broad overview of the code is available on the 
`main website <https://www.bigdft.org>`_. The documentation here will guide 
you from installing the code to using it for scientific studies.

.. toctree::
   :maxdepth: 2
   :caption: Overview

   overview/functionality

.. toctree::
   :maxdepth: 2

   overview/package

.. toctree::
   :maxdepth: 2

   overview/publications

.. toctree::
   :maxdepth: 2

   overview/license

.. toctree::
   :maxdepth: 2
   :caption: Basic Usage

   users/install

.. toctree::
   :maxdepth: 1

   school/QuickStart.ipynb

.. toctree::
   :maxdepth: 1

   school/CMDStart.ipynb

.. toctree::
   :maxdepth: 2
   :caption: User Guide

   users/guide

.. toctree::
   :maxdepth: 1

   PyBigDFT API <https://l_sim.gitlab.io/bigdft-suite/PyBigDFT/build/html/index.html>

.. toctree::
   :maxdepth: 2
   :caption: Developer

   devel/developers

.. toctree::
   :maxdepth: 1

   Futile API <https://l_sim.gitlab.io/bigdft-suite/futile/build/html/index.html>

Old Website
-----------

The documentation is under construction and some content is still available
from the old website. See the link `old website <https://old.bigdft.org/Wiki/?title=BigDFT_website>`_.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

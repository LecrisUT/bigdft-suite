#ifndef WRAPPER_MPI_H
#define WRAPPER_MPI_H

#include "futile_cst.h"
#include "FRefcnts"
#include "Dict"

namespace Futile {

class MpiEnvironment
{
public:

  F_DEFINE_TYPE(mpi_environment);
  operator f90_mpi_environment*() { return F_TYPE(mMpiEnvironment); };
  operator const f90_mpi_environment*() const { return F_TYPE(mMpiEnvironment); };
  operator f90_mpi_environment_pointer*() { return &mMpiEnvironment; };
  MpiEnvironment(f90_mpi_environment_pointer other) : mMpiEnvironment(other) {};

  MpiEnvironment(const MpiEnvironment& other);

  MpiEnvironment(const FReferenceCounter& refcnt,
    const int (*igroup) = nullptr,
    const int (*iproc) = nullptr,
    const int (*mpi_comm) = nullptr,
    const int (*ngroup) = nullptr,
    const int (*nproc) = nullptr);

  ~MpiEnvironment(void);

  MpiEnvironment(const int (*comm) = nullptr);

  void release_mpi_environment(void);

  void set(int iproc,
    int nproc,
    int mpi_comm,
    int groupsize);

  void dict(Dictionary& dict_info) const;

  void set1(int iproc,
    int mpi_comm,
    int groupsize,
    int ngroup);

private:
  f90_mpi_environment_pointer mMpiEnvironment;

};


}
#endif

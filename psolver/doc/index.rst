.. _psolver_index:

Welcome to psolver's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Modules:

   Poisson_Solver

.. toctree::
   :hidden:

   PSBase
   PSTypes
   PSBox

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Continuous integration
----------------------

.. image:: https://gitlab.com/l_sim/psolver/badges/devel/pipeline.svg
   :target: https://gitlab.com/l_sim/psolver/-/commits/devel

.. image:: https://gitlab.com/l_sim/psolver/badges/devel/coverage.svg
   :target: https://l_sim.gitlab.io/psolver/report

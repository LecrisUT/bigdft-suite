import yaml
input_QMQM = yaml.load(open('waterQMQM_base.yaml', 'r').read(),
                       Loader=yaml.Loader)
output_QM = yaml.load(open('log-waterQM.yaml', 'r').read(),
                      Loader=yaml.Loader)
input_QMQM['dft']['external_potential'] = output_QM['Multipole coefficients']
f = open('waterQMQM.yaml', 'w')
f.write(yaml.dump(input_QMQM))
